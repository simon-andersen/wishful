<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Adgangskoder skal være mindst otte tegn og matche hinanden.',
    'reset' => 'Dit kodeord er blevet nulstillet!',
    'sent' => 'Vi har sendt dig en email med et link til at nulstille din adgangskode.',
    'token' => 'Denne adgangskode til nulstilling af adgangskode er ugyldig.',
    'user' => "Vi kan ikke finde en bruger med denne e-mail.",

];
