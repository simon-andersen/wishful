/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import'./bootstrap';

// Import theme scripts
import'./theme/layout';

// Import common scripts
import'./common/global';