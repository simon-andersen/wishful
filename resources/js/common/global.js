$(document).ready(function() {
		$("#alert").fadeTo(3000, 700).slideUp(700, function() {
			$("#alert").slideUp(700);
	});
});

// Tooltip

$('button').tooltip({
	trigger: 'click',
	placement: 'bottom'
});

function setTooltip(btn, message) {
	$(btn).tooltip('hide')
			.attr('data-original-title', message)
			.tooltip('show');
}

function hideTooltip(btn) {
	setTimeout(function() {
		$(btn).tooltip('hide');
	}, 1500);
}

// Clipboard
import ClipboardJS from 'clipboard';
var clipboard = new ClipboardJS('.cb-btn');

clipboard.on('success', function(e) {
	setTooltip(e.trigger, 'Kopieret!');
	hideTooltip(e.trigger);
});

clipboard.on('error', function(e) {
	setTooltip(e.trigger, 'Fejlet!');
	hideTooltip(e.trigger);
});


// Flatpickr
import flatpickr from 'flatpickr';
import { Danish } from "flatpickr/dist/l10n/da.js"


flatpickr(".flatpickr-input", {
	dateFormat: "d-m-Y",
	maxDate: new Date(),
	"locale": Danish,
	disableMobile: "true",
});