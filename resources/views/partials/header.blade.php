<header class="header header-transparent" id="header-main">
    @if(Session::has('success'))
        <div class="bg-success py-2 text-white text-center text-sm position-relative zindex-100" id="alert">
            {!! Session::get("success")  !!}
        </div>
    @endif
    @if(Session::has('error'))
        <div class="bg-danger py-2 text-white text-center text-sm position-relative zindex-100" id="alert">
            {!! Session::get("error")  !!}
        </div>
@endif
<!-- Main navbar -->
    <nav class="navbar navbar-main navbar-expand-lg navbar-transparent @if($mode === 'dark') navbar-dark @else navbar-white @endif bg-dark"
         id="navbar-main">
        <div class="container px-lg-0">
            <!-- Logo -->
            <a class="navbar-brand mr-lg-5" href="/">
                <span class="h3 m-0 font-weight-bold @if($mode === 'dark') text-white @else text-dark @endif">Wishful</span>
            </a>
            <!-- Navbar collapse trigger -->
            <button class="navbar-toggler pr-0 collapsed" type="button" data-toggle="collapse"
                    data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Navbar nav -->
            <div class="navbar-collapse collapse text-center" id="navbar-main-collapse" style="">
                @if (!Auth::check())
                    <ul class="navbar-nav align-items-lg-center mt-4 mt-lg-0">
                        <!-- Home - Overview  -->
                        <li class="nav-item">
                            <a class="nav-link" href="/">Forside</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Support</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Om Wishful</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">Kontakt</a>
                        </li>
                    </ul>
                    @if ($mode === 'dark')
                        <ul class="navbar-nav align-items-lg-center ml-lg-auto d-flex flex-row justify-content-center mt-5 mt-lg-0">
                            <li class="btn-item nav-item mr-4 mr-lg-2">
                                <a href="{{ route('login') }}"
                                   class="btn btn-sm btn-outline-white rounded-pill btn-icon rounded-pill px-5 px-lg-4">
                                    <span class="btn-inner--text">{{ __('Log ind') }}</span>
                                </a>
                            </li>
                            <li class="btn-item nav-item">
                                <a href="{{ route('register') }}"
                                   class="btn btn-sm btn-white rounded-pill btn-icon rounded-pill px-5 px-lg-4">
                                    <span class="btn-inner--text">{{ __('Tilmeld') }}</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                @else
                    <ul class="navbar-nav align-items-lg-center mt-5 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link {{ Request::routeIs('home') ? 'active' : '' }}"
                               href="{{ route('home') }}">{{ __('Mine ønskelister') }}</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav align-items-lg-center ml-lg-auto d-none d-lg-flex">
                        <!-- Log out -->
                        <a href="{{ route('logout') }}" class="nav-link"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Log ud') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <!-- User profile -->
                        <a class="nav-link pr-0" href="{{ route('profile') }}">
                            <i class="far fa-user-circle fa-2x"></i>
                        </a>
                    </ul>
                    <ul class="navbar-nav align-items-lg-center ml-lg-auto d-flex d-lg-none flex-row justify-content-center mt-5 mt-lg-0">
                        <!-- User profile -->
                        <a href="{{ route('profile') }}"
                           class="btn btn-sm btn-white rounded-pill btn-icon rounded-pill px-5"><span
                                    class="btn-inner--icon"><i class="far fa-user-circle"></i></span> <span
                                    class="btn-inner--text d-none d-sm-inline-block">{{ __('Profil') }}</span>
                        </a>
                        <!-- Log out -->
                        <a href="{{ route('logout') }}"
                           class="btn btn-sm btn-outline-neutral rounded-pill btn-icon rounded-pill px-5"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span
                                    class="btn-inner--icon"><i class="fas fa-sign-out-alt"></i></span> <span
                                    class="btn-inner--text d-none d-sm-inline-block">{{ __('Log ud') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </ul>
                @endif
            </div>
        </div>
    </nav>
</header>