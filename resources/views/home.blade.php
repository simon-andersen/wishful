@extends('layouts.app')

@section('content')
    <!-- Header -->
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <div class="container pt-4 pt-lg-0">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Greeting -->
                    <div class="d-flex flex-column mb-4">
                        <span class="h2 mb-0 text-white d-block">{{ __('Hej,') }} {{Auth::user()->name}}</span>
                        <span class="text-white">{{ __("Ha' en god dag!") }}</span>
                    </div>
                    <!-- Navigation -->
                    <div class="d-flex">
                        <a href="{{ route('profile') }}" class="btn btn-icon btn-group-nav shadow btn-neutral">
                            <span class="btn-inner--icon"><i class="far fa-user-circle"></i></span>
                            <span class="btn-inner--text d-none d-md-inline-block">{{ __('Min profil') }}</span>
                        </a>
                        <div class="btn-group-nav ml-auto" role="group">
                            <a href="{{ route('wishlists.create') }}" class="btn btn-icon shadow btn-neutral">
                                <span class="btn-inner--icon"><i class="fas fa-plus-circle fa-lg"></i></span>
                                <span class="btn-inner--text d-none d-sm-inline-block">{{ __('Opret ønskeliste') }}</span>
                            </a>
                            <button data-target=".create-wish" data-toggle="modal"
                                    class="btn btn-icon shadow btn-warning mr-0">
                                <span class="btn-inner--icon"><i class="fas fa-plus-circle fa-lg"></i></span>
                                <span class="btn-inner--text d-none d-sm-inline-block">{{ __('Opret ønske') }}</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Wishlists -->
    @include('wishlists.index')
    <!-- Create wish modal -->
    <div class="modal fade create-wish" tabindex="-1" role="dialog"
         aria-labelledby="create-wish" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <button type="button" class="btn btn-secondary modal-close m-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="p-5">
                    <div class="row">
                        <div class="col-12 col-lg-5 text-center">
                            <h4>{{ __('Opret automatisk') }}</h4>
                            <div class="mb-4">{{ __('Indsæt et link og hent automatisk information om ønsket') }}</div>
                            <form method="POST" action="{{ route('wishes.create', ['id' => false]) }}">
                                @csrf
                                <div class="d-flex align-items-center mb-2">
                                    <input type="text" id="link" class="form-control" name="link"
                                           placeholder="{{__('Indsæt link') }}" required>
                                    <button type="submit" class="btn btn-warning ml-3">{{ __('Næste') }}</button>
                                </div>
                            </form>
                        </div>
                        <hr class="divider divider-vertical h-auto d-none d-lg-block my-0" />
                        <hr class="divider divider-horizontal d-block d-lg-none my-5" />
                        <div class="col-12 col-lg-5 text-center">
                            <h4>{{ __('Opret manuelt') }}</h4>
                            <div class="mb-4">{{ __('Opret manuelt, ved selv at indtaste information om ønsket') }}</div>
                            <a href="{{ route('wishes.create') }}" class="btn btn-warning">{{ __('Opret manuelt') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
