@extends('layouts.app')

@section('content')
    <!-- Header -->
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <div class="container pt-4 pt-lg-0">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Title -->
                    <div class="d-flex justify-content-between align-items-center mb-5">
                        <div>
                            <span class="h2 mb-0 text-white d-block">{{ __('Hej,') }} {{ $user->name }}</span>
                            <span class="text-white">{{ __("Ha' en god dag!") }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Edit user -->
    <section class="slice slice-lg delimiter-bottom bg-section-secondary pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <!-- General information form -->
                    <div>
                        <div class="actions-toolbar py-2 mb-4">
                            <h5 class="mb-1">{{ __('Profiloplysninger') }}</h5>
                        </div>
                        <form method="POST" action="{{ route('users.update', ['id' => $user->id]) }}">
                            @method('PATCH')
                            @csrf
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="name" class="form-control-label">{{ __('Navn') }}</label>
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $user->name }}" required>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="email" class="form-control-label">{{ __('E-mail') }}</label>
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ $user->email }}" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="date_of_birth"
                                           class="form-control-label">{{ __('Fødselsdag') }}</label>
                                    <input id="date_of_birth" type="date" class="form-control flatpickr-input" name="date_of_birth"
                                           value="{{ $user->date_of_birth }}" required>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label class="form-control-label mr-3">{{ __('Køn') }}</label>
                                    <div style="margin-top: -3.5px">
                                        <div class="custom-control custom-radio">
                                            <input class="custom-control-input" type="radio" id="gender-m" value="m"
                                                   name="gender" @if($user->gender === 'm') checked @endif>
                                            <label class="custom-control-label"
                                                   for="gender-m"> {{ __('Mand') }}</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input class="custom-control-input" type="radio" id="gender-f" value="f"
                                                   name="gender" @if($user->gender === 'f') checked @endif>
                                            <label class="custom-control-label"
                                                   for="gender-f">{{ __('Kvinde') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4">
                                <button type="submit"
                                        class="btn btn-sm btn-primary">{{ __('Opdatér profiloplysninger') }}</button>
                            </div>
                        </form>
                    </div>
                    <!-- Update password form -->
                    <div class="mt-5 pt-5">
                        <div class="actions-toolbar py-2 mb-4">
                            <h5 class="mb-1">{{ __('Ændre adgangskode') }}</h5>
                        </div>
                        <form method="POST" action="{{ route('users.update-password', ['id' => $user->id]) }}">
                            @method('PATCH')
                            @csrf
                            <div class="row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="password" class="form-control-label">{{ __('Adgangskode') }}</label>
                                    <input id="password" type="password" class="form-control" name="password"
                                           value="{{ old('password') }}">
                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label for="password_confirmation"
                                           class="form-control-label">{{ __('Bekræft adgangskode') }}</label>
                                    <input id="password_confirmation" type="password" class="form-control"
                                           name="password_confirmation">
                                </div>
                            </div>
                            <div class="mt-4">
                                <button type="submit"
                                        class="btn btn-sm btn-primary">{{ __('Opdatér adgangskode') }}</button>
                            </div>
                        </form>
                    </div>
                    <!-- Delete account form -->
                    <div class="mt-5 pt-5 delimiter-top">
                        <div class="actions-toolbar py-2 mb-4">
                            <h5 class="mb-1">{{ __('Slet bruger') }}</h5>
                            <p class="text-sm text-muted mb-0">{{ __('At slette din bruger betyder, at al data forsvinder.') }}</p>
                        </div>
                        <form method="POST" class="delete action-item p-0 mr-2" id="form"
                              onclick="return confirm('{{ __('Er du sikker du ønsker at slette din bruger?') }}')"
                              action="{{ route('users.destroy', ['id' => $user->id]) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger">{{ __('Slet bruger') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection