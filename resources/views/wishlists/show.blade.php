@extends('layouts.app')

@section('content')
    <!-- Header -->
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <div class="container pt-4 pt-lg-0">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Title -->
                    <div class="d-flex justify-content-between align-items-center mb-4">
                        <div>
                            <span class="h2 mb-0 text-white d-block">{{ $wishlist->name }}</span>
                            <span class="text-white">{{ __('Ønskeliste') }}</span>
                        </div>
                        <div>
                            <span class="mr-2 text-white d-none d-sm-inline-block opacity-7">{{ __('Del ønskelisten:') }}</span>
                            <button data-clipboard-action="copy" data-clipboard-text="{{URL::to('/share/'.$wishlist->share_id)}}" class="cb-btn icon icon-primary icon-shape icon-sm py-0 mr-1"><i class="fas fa-link"></i></button>
                        </div>
                    </div>
                    <!-- Navigation -->
                    <div class="d-flex">
                        <div class="btn-group-nav" role="group">
                            <a href="{{ route('wishlists.edit', ['id' => $wishlist->id]) }}"
                               class="btn btn-icon shadow btn-neutral">
                                <span class="btn-inner--icon"><i class="far fa-edit"></i></span>
                                <span class="btn-inner--text d-none d-md-inline-block">{{ __('Rediger') }}</span>
                            </a>
                            <form method="POST" class="delete action-item p-0 mr-2" id="form"
                                  onclick="return confirm('{{ __('Er du sikker du ønsker at slette ønskelisten?') }}')"
                                  action="{{ route('wishlists.destroy', ['id' => $wishlist->id]) }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-icon shadow btn-neutral">
                                    <span class="btn-inner--icon p-0"><i class="far fa-trash-alt"></i></span>
                                    <span class="btn-inner--text d-none d-md-inline-block">{{ __('Slet') }}</span>
                                </button>
                            </form>
                            <a href="{{route('wishlist.printpdf', ['id' => $wishlist->id])}}"
                               class="btn btn-icon shadow btn-neutral">
                                <span class="btn-inner--icon"><i class="fas fa-print"></i></span>
                                <span class="btn-inner--text d-none d-md-inline-block">{{ __('Print') }}</span>
                            </a>
                        </div>
                        <div class="btn-group-nav ml-auto" role="group">
                            <button data-target=".create-wish" data-toggle="modal"
                                    class="btn btn-icon shadow btn-warning mr-0">
                                <span class="btn-inner--icon"><i class="fas fa-plus-circle fa-lg"></i></span>
                                <span class="btn-inner--text d-none d-sm-inline-block">{{ __('Opret ønske') }}</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Wishes -->
    @include('wishes.index')
    <!-- Create wish modal -->
    <div class="modal fade create-wish" tabindex="-1" role="dialog"
         aria-labelledby="create-wish" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <button type="button" class="btn btn-secondary modal-close m-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="p-5">
                    <div class="row">
                        <div class="col-12 col-lg-5 text-center">
                            <h4>{{ __('Opret automatisk') }}</h4>
                            <div class="mb-4">{{ __('Indsæt et link og hent automatisk information om ønsket.') }}</div>
                            <form method="POST" action="{{ route('wishes.create') }}">
                                @csrf
                                <div class="d-flex align-items-center mb-2">
                                    <input name="wishlist" type="hidden" value="{{ $wishlist->id }}">
                                    <input type="text" id="link" class="form-control" name="link"
                                           placeholder="{{__('Indsæt link') }}" required>
                                    <button type="submit" class="btn btn-warning ml-3">{{ __('Næste') }}</button>
                                </div>
                            </form>
                        </div>
                        <hr class="divider divider-vertical h-auto d-none d-lg-block my-0"/>
                        <hr class="divider divider-horizontal d-block d-lg-none my-5"/>
                        <div class="col-12 col-lg-5 text-center">
                            <h4>{{ __('Opret manuelt') }}</h4>
                            <div class="mb-4">{{ __('Opret manuelt, ved selv at indtaste information om ønsket.') }}</div>
                            <form method="POST" action="{{ route('wishes.create') }}">
                                @csrf
                                <input name="wishlist" type="hidden" value="{{ $wishlist->id }}">
                                <button type="submit" class="btn btn-warning ml-3">{{ __('Opret manuelt') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection