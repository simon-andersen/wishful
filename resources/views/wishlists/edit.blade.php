@extends('layouts.app')

@section('content')
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <!-- Circles -->
        @include('partials.circles')
        <!-- Header  -->
        <div class="container mb-5">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Title -->
                    <div class="row align-items-center mb-4">
                        <div class="col-md-5 mb-4 mb-md-0">
                            <span class="h2 mb-0 text-white d-block">{{ __('Rediger ønskeliste') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Edit wishlist  -->
    <section class="slice bg-section-secondary">
        <div class="container mn-5">
            <div class="row">
                <div class="col-12">
                    <div class="card zindex-100">
                        <div class="card-body px-md-5 py-5">
                            <form method="POST" action="{{ route('wishlists.update', ['id' => $wishlist->id]) }}">
                                @method('PATCH')
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-7">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label">{{ __('Navn på ønskeliste (påkrævet)') }}</label>
                                            <input type="text" id="name" class="form-control" name="name" value="{{ $wishlist->name }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="form-control-label">{{ __('Beskrivelse af ønskeliste') }}</label>
                                            <textarea  id="description" class="form-control" name="description">{{ $wishlist->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-warning">{{ __('Gem') }}</button>
                                    <button type="button" class="btn btn-link text-muted">{{ __('Annuller') }}</button>
                                </div>
                                @if ($errors->any())
                                    <div class="small text-danger mt-3" role="alert">
                                        <strong>{{ $errors->first() }}</strong>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection