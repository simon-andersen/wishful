<section class="slice bg-section-secondary">
    <div class="container">
        <div class="actions-toolbar py-2 mb-4">
            <h5 class="mb-1">{{ __('Mine ønskelister') }}</h5>
            <p class="text-sm text-muted mb-0">{{ __('Opret, rediger og slet ønskelister') }}</p>
        </div>
        <div class="row">
            @forelse($wishlists as $wishlist)
                <div class="col-12 col-sm-6 col-lg-4 d-flex">
                    <div class="wishlist__item card hover-shadow-lg hover-translate-y-n3 flex-fill">
                        <div class="card-header text-center">
                            <h5 class="my-2"><a
                                        href="{{ route('wishlists.show', ['id' => $wishlist->id]) }}">{{ $wishlist->name }}</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            @forelse($wishlist->wishes->reverse()->take(2) as $wish)
                                <div class="row flex-row @if(!$loop->last) mb-3 @endif">
                                    <div class="col-auto">
                                        <div class="avatar rounded-circle">
                                            @if($wish->image)
                                                <img class="h-100"
                                                     src="{{ asset($wish->image) }}">
                                            @else
                                                <img class="h-100 opacity-5"
                                                     src="{{ asset('images/svg/illustrations/gift.svg') }}">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col d-flex mb-2 align-items-center">
                                        <div class="h6 text-sm mb-0">{{ str_limit($wish->name, 30, '...') }}</div>
                                    </div>
                                </div>
                            @empty
                                <div class="d-flex flex-column text-center align-items-center h-100 text-sm">
                                    {{ __('Der er endnu ingen ønsker oprettet.') }}
                                    <button data-target=".create-wish" data-toggle="modal" class="btn btn-outline-warning btn-sm mt-3"> {{ __('Opret nyt ønske') }}</button>
                                </div>
                            @endforelse
                            @if($wishlist->wishes->count() > 2)
                                <div class="text-muted text-sm mt-3">{{ $wishlist->wishes->count() - 2 }} @if(($wishlist->wishes->count() - 2) === 1) {{__('ønske')}} @else {{__('ønsker')}} @endif {{__('mere ...')}}</div>
                            @endif
                        </div>
                        <div class="card-footer">
                            <div class="actions d-flex justify-content-between">
                                <div>
                                    <form method="POST" class="delete action-item" id="form"
                                          onclick="return confirm('{{ __('Er du sikker du ønsker at slette ønskelisten?') }}')"
                                          action="{{ route('wishlists.destroy', ['id' => $wishlist->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="action-item text-danger">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </form>
                                </div>
                                <strong class="h6 text-sm d-flex align-items-center m-0 pr-2">
                                    <a href="{{ route('wishlists.show', ['id' => $wishlist->id]) }}">{{ __('Se ønskeliste') }}</a>
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12 col-sm-6 col-lg-4 d-flex">
                    <div class="card flex-fill alert alert-warning" role="alert">
                        <div class="p-3">
                            <h5 class="alert-heading">
                                Velkommen!
                            </h5>
                            <div class="font-weight-bold mt-3">Kom godt igang med Wishful.</div>
                            <div class="my-3">Opret din første ønskeliste ... til dig selv, dine børn eller en helt
                                tredje.
                            </div>
                            <a href="{{ route('wishlists.create') }}" class="btn btn-sm btn-warning">Opret
                                ønskeliste</a>
                        </div>
                    </div>
                </div>
            @endforelse
            @if(!$wishlists->count() == 0)
                <div class="col-12 col-sm-6 col-lg-4 d-flex">
                    <a href="{{ route('wishlists.create') }}"
                       class="wishlist__item card border-md border-dashed bg-transparent shadow-none flex-fill">
                        <div class="card-body text-center d-flex flex-column align-items-center justify-content-center p-5">
                            <button type="button" class="btn btn-soft-dark rounded-circle btn-icon-only mr-0">
                                <span class="btn-inner--icon">
                                    <i class="fas fa-plus"></i>
                                </span>
                            </button>
                            <div class="mt-4 h6 text-muted bg-gra">{{ __('Opret ny ønskeliste') }}</div>
                        </div>
                    </a>
                </div>
            @endif
        </div>
    </div>
</section>