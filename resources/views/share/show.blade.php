@extends('layouts.app')

@section('content')
    <!-- Header -->
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <div class="container pt-4 pt-lg-0">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Title -->
                    <div class="row align-items-center mb-4">
                        <div class="col-md-5">
                            <span class="h2 mb-0 text-white d-block">{{ $wishlist->name }}</span>
                            <span class="text-white">{{ __('Ønskeliste delt af ') }} @foreach($wishlist->users as $user) {{ $user->name }} @if(!$loop->last)
                                    - @endif @endforeach</span>
                        </div>
                        <div class="col-md-7 flex-fill d-none d-md-block text-right text-white">
                            {!! $wishlist->description !!}
                        </div>
                    </div>
                    <!-- Print -->
                    <div class="d-flex">
                        <div class="btn-group btn-group-nav shadow ml-auto" role="group" aria-label="Basic example">
                            <div class="btn-group" role="group">
                                <a href="{{route('wishlist.printpdf', ['id' => $wishlist->id])}}"
                                   class="btn btn-icon shadow btn-neutral">
                                    <span class="btn-inner--icon"><i class="fas fa-print"></i></span>
                                    <span class="btn-inner--text d-none d-md-inline-block">{{ __('Print') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Wishes -->
    <section class="slice slice-lg delimiter-bottom bg-section-secondary pt-5">
        <div class="container">
            <div class="actions-toolbar py-2 mb-4">
                <h5 class="mb-1">{{ __('Ønsker') }}</h5>
            </div>
            <div class="row">
                @foreach($wishlist->wishes as $wish)
                    <div class="modal fade wish-{{$wish->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="wish-{{$wish->id}}" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <button type="button" class="btn btn-secondary modal-close m-0" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12 col-lg-5">
                                            @if($wish->image)
                                                <div class="bg-img-lg">
                                                    <img src="{!! asset($wish->image) !!}"
                                                         class="img-center img-fluid">
                                                </div>
                                            @else
                                                <div class="bg-img-lg">
                                                    <img src="{{asset('images/svg/illustrations/gift.svg')}}"
                                                         class="opacity-5 img-placeholder">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-lg-7 d-flex flex-column">
                                            <div class="h3 my-4 mt-lg-0">{{ $wish->name }}</div>
                                            @isset($wish->description)
                                                <p class="mb-4">
                                                    {{ $wish->description }}
                                                </p>
                                            @endisset
                                            @isset($wish->price)
                                                <div class="card-price mb-4"><span class="font-weight-bold h6">{{ $wish->price }}
                                                        <span class="text-sm">{{ __('kr') }}</span></span></div>
                                            @endisset
                                            <div class="d-flex mt-auto justify-content-end">
                                                @if (Auth::check())
                                                    @if($wish->reserved)
                                                        <form method="POST"
                                                              action="{{ route('share.delete-reserve', ['id' => $wish->id]) }}">
                                                            @method('PATCH')
                                                            @csrf
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-warning">{{ __('Slet reservering ') }}</button>
                                                        </form>
                                                    @else
                                                        <form method="POST"
                                                              action="{{ route('share.reserve', ['id' => $wish->id]) }}">
                                                            @method('PATCH')
                                                            @csrf
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-primary">{{ __('Reservér ønske') }}</button>
                                                        </form>
                                                    @endif
                                                @else
                                                    @if($wish->reserved)
                                                        <button class="btn btn-sm btn-warning">{{ __('Ønsket er reserveret') }}</button>
                                                    @else
                                                        <button class="btn btn-sm btn-primary"
                                                                data-target=".register"
                                                                data-toggle="modal" data-dismiss="modal"
                                                                aria-label="Close">{{ __('Reservér ønske') }}
                                                        </button>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 col-xl-3 d-flex">
                        <div class="wish__item card hover-shadow-lg hover-translate-y-n3 flex-fill">
                            @if($wish->image)
                                <div class="bg-img m-4">
                                    <img src="{!! asset($wish->image) !!}"
                                         class="img-center img-fluid">
                                </div>
                            @else
                                <div class="bg-img m-4">
                                    <img src="{{asset('images/svg/illustrations/gift.svg')}}"
                                         class="opacity-5 img-placeholder">
                                </div>
                            @endif
                            <div class="card-body text-center d-flex flex-column pt-0">
                                <strong class="h6 m-0 p-0 pb-2">
                                    <a href="#" data-target=".wish-{{$wish->id}}"
                                       data-toggle="modal">{{ $wish->name }}</a>
                                </strong>
                                @isset($wish->description)
                                    <p class="text-sm">
                                        {{ str_limit($wish->description, 50, '...') }}
                                    </p>
                                @endisset
                                @isset($wish->price)
                                    <div class="card-price mt-auto"><span class="font-weight-bold h6">{{ $wish->price }}
                                            <span class="text-sm">{{ __('kr') }}</span></span></div>
                                @endisset
                            </div>
                            <div class="card-footer py-2">
                                <div class="actions d-flex flex-column text-center py-3">
                                    <a href="#" class="h6 btn btn-sm btn-link mr-0 mb-3 pt-0"
                                       data-target=".wish-{{$wish->id}}"
                                       data-toggle="modal">{{ __('Se ønske') }}
                                    </a>
                                    @if (Auth::check())
                                        @if($wish->reserved)
                                            <form method="POST"
                                                  action="{{ route('share.delete-reserve', ['id' => $wish->id]) }}">
                                                @method('PATCH')
                                                @csrf
                                                <button type="submit"
                                                        class="btn btn-sm btn-warning w-100">{{ __('Slet reservering ') }}</button>
                                            </form>
                                        @else
                                            <form method="POST"
                                                  action="{{ route('share.reserve', ['id' => $wish->id]) }}">
                                                @method('PATCH')
                                                @csrf
                                                <button type="submit"
                                                        class="btn btn-sm btn-primary w-100">{{ __('Reservér ønske') }}</button>
                                            </form>
                                        @endif
                                    @else
                                        @if($wish->reserved)
                                            <button class="btn btn-sm btn-warning w-100">{{ __('Ønsket er reserveret') }}</button>
                                        @else
                                            <button class="btn btn-sm btn-primary w-100"
                                                    data-target=".register"
                                                    data-toggle="modal">{{ __('Reservér ønske') }}
                                            </button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Reserve wish modal -->
    <div class="modal fade register" tabindex="-1" role="dialog"
         aria-labelledby="register" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <button type="button" class="btn btn-secondary modal-close m-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="p-5 text-center">
                    <div>{{ __('Beklager... Du skal være tilmeldt og logget ind i Wishful, for at kunne reservere ønsket.') }}</div>
                    <div>{{ __('Klik på knappen for nemt og hurtigt at tilmelde dig Wishful.') }}</div>
                    <a href="{{ route('register') }}" class="btn btn-sm btn-primary mt-3">{{ __('Tilmeld dig nu') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection