@extends('layouts.app')

@section('content')
    <section class="slice slice-lg bg-primary vh-100" data-offset-top="#header-main" style="padding-top: 147.188px;">
        <!-- Circles -->
    @include('partials.circles')
    <!-- Hero container -->
        <div class="container h-100 d-flex align-items-center position-relative zindex-100">
            <div class="col">
                <div class="row justify-content-center">
                    <div class="col-lg-7 text-center">
                        <img class="w-50" src="{{ asset('images/svg/illustrations/403.svg') }}">
                        <h2 class="my-5 font-weight-400 text-white">
                            @if($exception->getMessage())
                                {{ $exception->getMessage() }}
                            @else
                                {{ __('Du har ikke adgang til at se denne side.') }}
                            @endif
                        </h2>
                        <a href="/" class="btn btn-white btn-icon rounded-pill hover-translate-y-n3">
                            <span class="btn-inner--text">{{ __('Gå tilbage til forsiden') }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection