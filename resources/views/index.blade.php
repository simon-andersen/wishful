@extends('layouts.app')

@section('content')
    <section class="slice slice-lg bg-gradient-primary" data-offset-top="#header-main" style="padding-top: 147.188px;">
        <!-- Circles -->
    @include('partials.circles')
    <!-- Hero container -->
        <div class="container position-relative zindex-100">
            <div class="row row-grid justify-content-between align-items-center">
                <div class="offset-xl-3 col-xl-6 align-self-end">
                    <div class="py-6 py-lg-8 text-center">
                        <h1 class="text-white mb-4">Opret en digital ønskeliste, og modtag bedre gaver</h1>
                        <p class="font-size-lg lh-180 text-white lead">Med Wishful er det nemt at <b>oprette ønskelister</b> og
                            <b>hurtigt dele dem</b> med venner og familie</p>
                        <div class="mt-5">
                            <a href="/register" class="btn btn-warning rounded-pill mr-lg-4 mb-4 mb-md-0">
                                <span>{{ __('Opret din ønskeliste') }}</span>
                            </a>
                            <a href="#read-more" class="btn btn-outline-white btn-icon rounded-pill" data-scroll-to="">
                                <span class="btn-inner--text">{{ __('Læs mere') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="shape-container" data-shape-position="bottom" style="height: 198px;">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1600 220" preserveAspectRatio="none"
                 class="ie-shape-wave-3">
                <path d="M918.34,99.41C388.23,343.6,47.11,117.12,0,87.54V220H1600V87.54C1378.72-76.71,1077.32,27.41,918.34,99.41Z"></path>
            </svg>
        </div>
    </section>
    <section class="slice slice-lg" id="read-more">
        <div class="container">
            <div class="row row-grid">
                <div class="col-lg-4">
                    <div class="text-center">
                        <div class="px-4 py-5">
                            <i class="fas fa-store fa-5x" style="color: #ffda91"></i>
                        </div>
                        <div class="px-4 pb-4">
                            <h5>
                                {{ __('Tilføj alting') }}
                            </h5>
                            <p class="text-muted">Tilføj gaver fra enhver butik, online eller fysisk.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="text-center">
                        <div class="px-4 py-5">
                            <i class="fas fa-birthday-cake fa-5x" style="color: #ffda91"></i>
                        </div>
                        <div class="px-4 pb-4">
                            <h5>
                                {{ __('Enhver anledning') }}
                            </h5>
                            <p class="text-muted">Fødselsdage, jul, bryllup, babyshower... Wishful fungerer godt hver
                                gang.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="text-center">
                        <div class="px-4 py-5">
                            <i class="fas fa-coins fa-5x" style="color: #ffda91"></i>
                        </div>
                        <div class="px-4 pb-4">
                            <h5>
                                {{ __('Helt gratis') }}
                            </h5>
                            <p class="text-muted">Wishful er helt gratis og helt ubegrænset.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice slice-xl overflow-hidden bg-gradient-dark delimiter-top delimiter-bottom">
    @include('partials.circles')
    <!-- Hero container -->
        <div class="container position-relative zindex-100">
            <div class="text-center">
                <h3 class="text-white">5.840 nye ønsker i denne uge</h3>
                <div class="text-white mt-3">
                    <p class="lead lh-180">Gør som tusinder af andre glade brugere, der opretter deres ønskelister
                        ...</p>
                    <a href="/register" class="btn btn-warning rounded-pill btn-icon mt-5">
                        <span class="btn-inner--text">{{__('Opret din ønskeliste')}}</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="slice pb-0 bg-section-secondary">
        <div class="container">
            <div class="row row-grid justify-content-around align-items-center">
                <div class="col-lg-5">
                    <div class="">
                        <h5 class="h3">Opret en ønskeliste</h5>
                        <p class="lead my-4">Opret en ønskeliste, og fyld den derefter med de gaver, du ønsker dig. Alle
                            ønsker indeholder detaljer og link til hvor den kan købes.</p>
                        <a href="/register" class="link link-underline-info font-weight-bold">Opret din første ønskeliste</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img alt="Image placeholder" src="{{asset('images/mockups/moc1.png')}}"
                         class="w-50 img-center">
                </div>
            </div>
        </div>
    </section>
    <section class="slice py-0 bg-section-secondary">
        <div class="container">
            <div class="row row-grid justify-content-around align-items-center">
                <div class="col-lg-5 order-lg-2">
                    <div class=" pr-lg-4">
                        <h5 class="h3">Del din ønskeliste</h5>
                        <p class="lead my-4">Del din ønskeliste med familie og venner. De har mulighed for at reservere
                            ønsker, så du undgår at få den samme gave to gange.</p>
                        <a href="/register" class="link link-underline-warning font-weight-bold">Opret en ønskeliste nu</a>
                    </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                    <img alt="Image placeholder" src="{{asset('images/mockups/moc2.png')}}"
                         class="w-50 img-center">
                </div>
            </div>
        </div>
    </section>
    <section class="slice pt-0 bg-section-secondary">
        <div class="container">
            <div class="row row-grid justify-content-around align-items-center">
                <div class="col-lg-5">
                    <div class="">
                        <h5 class="h3">Få de gaver, du ønsker</h5>
                        <p class="lead my-4">På grund af din fantastiske ønskeliste ved alle, hvad du kan lide. Og du er
                            derfor sikker på at få de gaver, du præcist ønsker.</p>
                        <a href="/register" class="link link-underline-info font-weight-bold">Kom i gang nu</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img alt="Image placeholder" src="{{asset('images/mockups/moc3.png')}}"
                         class="w-50 img-center">
                </div>
            </div>
        </div>
    </section>
    <section class="slice slice-lg bg-gradient-primary" id="sct-call-to-action">
        <a href="#sct-call-to-action" class="tongue tongue-up tongue-section-secondary" data-scroll-to="">
            <i class="fas fa-angle-up"></i>
        </a>
        <div class="container text-center">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-white">Kommende fødselsdag? Babyshower? Bryllup? </h1>
                    <div class="row justify-content-center mt-4">
                        <div class="col-lg-8">
                            <p class="lead text-white">
                                Der er så mange gode grunde til at bruge Wishful til din næste vigtige begivenhed.
                            </p>
                            <div class="btn-container">
                                <a href="/register" class="btn btn-warning rounded-pill btn-icon mt-5">
                                    <span class="btn-inner--text">Opret din første ønskeliste</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice slice-lg">
        <div class="container">
            <div class="mb-5 text-center">
                <h3>Dette siger vores kunder</h3>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <img alt="Image placeholder"
                                         src="{{asset('images/persons/person-3.jpg')}}"
                                         class="avatar  rounded-circle">
                                </div>
                                <div class="pl-3">
                                    <h5 class="h6 mb-0">Rebecca Caspersen</h5>
                                </div>
                            </div>
                            <p class="mt-4 lh-180">Det er en ny sjov måde at dele ønskelister med venner og familie.
                                Let at bruge, Wishful giver dig mulighed for at oprette og dele ønskelister til
                                alle lejligheder.</p>
                            <span class="static-rating static-rating-sm">
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <img alt="Image placeholder"
                                         src="{{asset('images/persons/person-2.jpg')}}"
                                         class="avatar  rounded-circle">
                                </div>
                                <div class="pl-3">
                                    <h5 class="h6 mb-0">Helle Winther</h5>
                                </div>
                            </div>
                            <p class="mt-4 lh-180">SÅ smart, når jeg tager mine børn ud for at købe gaver til deres
                                venner! Wishful hjælper mig med at holde orden på deres ønskelister.</p>
                            <span class="static-rating static-rating-sm">
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <img alt="Image placeholder"
                                         src="{{asset('images/persons/person-1.jpg')}}"
                                         class="avatar  rounded-circle">
                                </div>
                                <div class="pl-3">
                                    <h5 class="h6 mb-0">Anders Thomsen</h5>
                                </div>
                            </div>
                            <p class="mt-4 lh-180">Gratis og enkel. Jeg kan tilmelde mig, oprette flere ønskelister og
                                nemt dele dem på Facebook osv. Det fungerer også godt på min iPhone.</p>
                            <span class="static-rating static-rating-sm">
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                          <i class="star fas fa-star voted"></i>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('partials.footer')
@endsection