@extends('layouts.app', ['mode' => 'white'])

@section('content')
    <section class="bg-section-secondary min-vh-100" data-offset-top="#header-main">
        <!-- Circles -->
    @include('partials.circles')
    <!-- Hero container -->
        <div class="container py-5 position-relative zindex-100">
            <div class="row row-grid justify-content-center justify-content-lg-between align-items-center">
                <div class="col-sm-10 col-lg-8 col-xl-7 order-lg-2">
                    <div class="card shadow zindex-100 mb-0">
                        <div class="card-body px-md-5 py-5">
                            <h6 class="h3 mb-4">{{ __('Tilmeld') }}</h6>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-12 col-md-6">
                                        <label for="name" class="form-control-label">{{ __('Navn') }}</label>
                                        <input id="name" type="text" class="form-control" name="name"
                                               value="{{ old('name') }}" required>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="email" class="form-control-label">{{ __('E-mail') }}</label>
                                        <input id="email" type="email" class="form-control" name="email"
                                               value="{{ old('email') }}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12 col-md-6">
                                        <label for="password" class="form-control-label">{{ __('Adgangskode') }}</label>
                                        <input id="password" type="password" class="form-control" name="password"
                                               value="{{ old('password') }}" required>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="password_confirmation"
                                               class="form-control-label">{{ __('Bekræft adgangskode') }}</label>
                                        <input id="password_confirmation" type="password" class="form-control"
                                               name="password_confirmation" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12 col-md-6">
                                        <label for="date_of_birth"
                                               class="form-control-label">{{ __('Fødselsdag') }}</label>
                                        <input id="date_of_birth" class="form-control flatpickr-input" name="date_of_birth"
                                               value="{{ old('date_of_birth') }}" placeholder="{{ 'dd-mm-åååå' }}" required>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label class="form-control-label mr-3">{{ __('Køn') }}</label>
                                        <div style="margin-top: -3.5px">
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" id="gender-m" value="m"
                                                       name="gender" required>
                                                <label class="custom-control-label"
                                                       for="gender-m">{{ __('Mand') }}</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input class="custom-control-input" type="radio" id="gender-f" value="f"
                                                       name="gender">
                                                <label class="custom-control-label"
                                                       for="gender-f">{{ __('Kvinde') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-block btn-primary">{{ __('Tilmeld') }}</button>
                                </div>
                                @if ($errors->any())
                                    <div class="small text-danger mt-3" role="alert">
                                        <strong>{{ $errors->first() }}</strong>
                                    </div>
                                @endif
                            </form>
                        </div>
                        <div class="card-footer px-md-5">
                            <small>{{ __('Er du allerede bruger?') }}</small>
                            <a href="{{ route('login') }}" class="small font-weight-bold">{{ __('Log ind her') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 order-lg-1 d-none d-lg-block">
                    <div>
                        <h2 class="mb-4">
                            <span class="d-block">Den bedste </span>
                            <span class="display-4 font-weight-light">ønskeliste</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
