@extends('layouts.app', ['mode' => 'white'])

@section('content')
    <section class="bg-section-secondary min-vh-100" data-offset-top="#header-main">
    <!-- Circles -->
    @include('partials.circles')
    <!-- Hero container -->
        <div class="container py-5 position-relative zindex-100">
            <div class="row row-grid justify-content-center justify-content-lg-between align-items-center">
                <div class="col-sm-8 col-lg-6 col-xl-5 order-lg-2">
                    <div class="card shadow zindex-100 mb-0">
                        <div class="card-body px-md-5 py-5">
                            <div class="mb-5">
                                <h6 class="h3">{{ __('Log ind') }}</h6>
                            </div>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="email" class="form-control-label">{{ __('E-mail') }}</label>
                                    <input type="email" id="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </div>
                                <div class="form-group mb-4">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <label for="password" class="form-control-label">{{ __('Adgangskode') }}</label>
                                        <a href="{{ route('password.request') }}" class="mb-2 small text-muted text-underline--dashed border-primary">{{ __('Glemt adgangskode?') }}</a>
                                    </div>
                                    <input type="password" id="password" class="form-control form-control" name="password" required>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-block btn-primary">{{ __('Log ind') }}</button>
                                </div>
                                @if ($errors->any())
                                    <div class="small text-danger mt-3" role="alert">
                                        <strong>{{ $errors->first() }}</strong>
                                    </div>
                                @endif
                            </form>
                        </div>
                        <div class="card-footer px-md-5">
                            <small>{{ __('Er du ny bruger?') }}</small>
                            <a href="{{ route('register') }}" class="small font-weight-bold">{{ __('Tilmeld dig nu') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 order-lg-1 d-none d-lg-block">
                    <div>
                        <h2 class="mb-4">
                            <span class="d-block">Den bedste </span>
                            <span class="display-4 font-weight-light">ønskeliste</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
