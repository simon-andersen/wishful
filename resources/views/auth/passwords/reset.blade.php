@extends('layouts.app', ['mode' => 'white'])

@section('content')
    <section class="bg-section-secondary min-vh-100" data-offset-top="#header-main">
        <!-- Circles -->
    @include('partials.circles')
    <!-- Hero container -->
        <div class="container py-5 position-relative zindex-100">
            <div class="row row-grid justify-content-center justify-content-lg-between align-items-center">
                <div class="col-sm-8 col-lg-6 col-xl-5 order-lg-2">
                    <div class="card shadow zindex-100 mb-0">
                        <div class="card-body px-md-5 py-5">
                            <div class="mb-5">
                                <h6 class="h3">{{ __('Nulstil din adgangskode') }}</h6>
                            </div>
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf

                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group">
                                    <label for="email" class="form-control-label">{{ __('E-mail') }}</label>
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="form-control-label">{{ __('Ny adgangskode') }}</label>
                                    <input id="password" type="password" class="form-control" name="password"
                                           value="{{ old('password') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation"
                                           class="form-control-label">{{ __('Bekræft adgangskode') }}</label>
                                    <input id="password_confirmation" type="password" class="form-control"
                                           name="password_confirmation" required>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-block btn-primary">{{ __('Nulstil adgangskode') }}</button>
                                </div>
                                @if ($errors->any())
                                    <div class="small text-danger mt-3" role="alert">
                                        <strong>{{ $errors->first() }}</strong>
                                    </div>
                                @endif
                                @if (session('status'))
                                    <div class="small text-success mt-4" role="alert">
                                        <strong>{{ session('status') }}</strong>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 order-lg-1 d-none d-lg-block">
                    <div>
                        <h2 class="mb-4">
                            <span class="d-block">Den bedste </span>
                            <span class="display-4 font-weight-light">ønskeliste</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
