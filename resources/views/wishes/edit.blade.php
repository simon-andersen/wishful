@extends('layouts.app')

@section('content')
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <!-- Circles -->
    @include('partials.circles')
    <!-- Header -->
        <div class="container mb-5">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Title -->
                    <div class="row align-items-center mb-4">
                        <div class="col-md-5 mb-4 mb-md-0">
                            <span class="h2 mb-0 text-white d-block">{{ __('Rediger ønske') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Create wish  -->
    <section class="slice bg-section-secondary">
        <div class="container mn-5">
            <div class="row">
                <div class="col-12">
                    <div class="card zindex-100">
                        <div class="card-body px-md-5 py-5">
                            <form method="POST" action="{{ route('wishes.update', ['id' => $wish->id]) }}">
                                @method('PATCH')
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-5">
                                        <div class="form-group">
                                            <label for="wish_list_id" class="form-control-label">{{ __('Vælg ønskeliste') }}</label>
                                            <select name="wish_list_id" id="wish_list_id" class="custom-select" data-toggle="select" required>
                                                @foreach($wishlists as $wishlist)
                                                    <option value="{{ Crypt::encrypt($wishlist->id) }}" @if($wishlist->id === $wish->wish_list_id) selected @endif>{{ $wishlist->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-control-label">{{ __('Billede') }}</div>
                                        <div class="bg-img-lg my-3">
                                            <img src="{{$wish->image}}" class="img-center img-fluid">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label">{{ __('Titel') }}</label>
                                            <input type="text" id="name" class="form-control" name="name" value="{{ $wish->name }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="form-control-label">{{ __('Beskrivelse') }}</label>
                                            <textarea  id="description" class="form-control" name="description">{{ $wish->description }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="price" class="form-control-label">{{ __('Pris') }}</label>
                                            <input type="number" id="price" class="form-control" name="price" value="{{ $wish->price }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="link" class="form-control-label">{{ __('Link') }}</label>
                                            <input type="text" id="link" class="form-control" name="link" value="{{ $wish->link }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-warning">{{ __('Gem') }}</button>
                                    <a href="{{ URL::previous() }}"
                                       class="btn btn-link text-muted">{{ __('Annuller') }}</a>
                                </div>
                                @if ($errors->any())
                                    <div class="small text-danger mt-3" role="alert">
                                        <strong>{{ $errors->first() }}</strong>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection