<section class="slice slice-lg delimiter-bottom bg-section-secondary">
    <div class="container">
        <div class="actions-toolbar py-2 mb-4">
            <h5 class="mb-1">{{ __('Ønsker') }}</h5>
            <p class="text-sm text-muted mb-0">{{ __('Opret, rediger og slet ønsker.') }}</p>
        </div>
        <div class="row">
        @foreach($wishes as $wish)
            <!-- Large modal -->
                <div class="modal fade wish-{{$wish->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="wish-{{$wish->id}}" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <button type="button" class="btn btn-secondary modal-close m-0" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12 col-lg-5">
                                        @if($wish->image)
                                            <div class="bg-img-lg">
                                                <img src="{!! asset($wish->image) !!}"
                                                     class="img-center img-fluid">
                                            </div>
                                        @else
                                            <div class="bg-img-lg">
                                                <img src="{{asset('images/svg/illustrations/gift.svg')}}"
                                                     class="opacity-5 img-placeholder">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-12 col-lg-7 d-flex flex-column">
                                        <div class="h3 my-4 mt-lg-0">{{ $wish->name }}</div>
                                        @isset($wish->description)
                                            <p class="mb-4">
                                                {{ $wish->description }}
                                            </p>
                                        @endisset
                                        @isset($wish->price)
                                            <div class="card-price mb-4"><span class="font-weight-bold h6">{{ $wish->price }}
                                                    <span class="text-sm">{{ __('kr') }}</span></span></div>
                                        @endisset
                                        <div class="d-flex mt-auto justify-content-end">
                                            <form method="POST" class="delete action-item p-0 mr-2" id="form"
                                                  onclick="return confirm('{{ __('Er du sikker du ønsker at slette ønsket?') }}')"
                                                  action="{{ route('wishes.destroy', ['id' => $wish->id]) }}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-outline-primary"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </form>
                                            <a href="{{ route('wishes.edit', ['id' => $wish->id]) }}"
                                               class="btn btn-sm btn-outline-primary">
                                                {{ __('Rediger') }}
                                            </a>
                                            <a href="{{ $wish->link }}" target="_blank" class="btn btn-sm btn-outline-primary">
                                                {{ __('Link til ønske') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 col-xl-3 d-flex">
                    <div class="wish__item card hover-shadow-lg hover-translate-y-n3 flex-fill">
                        @if($wish->image)
                            <div class="bg-img m-4">
                                <img src="{!! asset($wish->image) !!}"
                                     class="img-center img-fluid">
                            </div>
                        @else
                            <div class="bg-img m-4">
                                <img src="{{asset('images/svg/illustrations/gift.svg')}}"
                                     class="opacity-5 img-placeholder">
                            </div>
                        @endif
                        <div class="card-body text-center d-flex flex-column pt-0">
                            <strong class="h6 m-0 p-0 pb-2">
                                <a href="#" data-target=".wish-{{$wish->id}}" data-toggle="modal">{{ $wish->name }}</a>
                            </strong>
                            @isset($wish->description)
                                <p class="text-sm">
                                    {{ str_limit($wish->description, 50, '...') }}
                                </p>
                            @endisset
                            @isset($wish->price)
                                <div class="card-price mt-auto"><span class="font-weight-bold h6">{{ $wish->price }}
                                        <span class="text-sm">{{ __('kr') }}</span></span></div>
                            @endisset
                        </div>
                        <div class="card-footer py-2">
                            <div class="actions d-flex justify-content-between">
                                <div>
                                    <a href="{{ route('wishes.edit', ['id' => $wish->id]) }}"
                                       class="delete action-item mr-2">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <form method="POST" class="delete action-item" id="form"
                                          onclick="return confirm('{{ __('Er du sikker du ønsker at slette ønsket?') }}')"
                                          action="{{ route('wishes.destroy', ['id' => $wish->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="action-item text-danger ml-2">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </form>
                                </div>
                                <strong class="h6 text-sm d-flex align-items-center m-0 pr-2">
                                    <a href="#" data-target=".wish-{{$wish->id}}"
                                       data-toggle="modal">{{ __('Se ønske') }}</a>
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-xl-3 col-lg-4 col-sm-6 d-flex">
                <div data-target=".create-wish" data-toggle="modal"
                   class="wish__item card border-md border-dashed bg-transparent shadow-none flex-fill" style="cursor: pointer;">
                    <div class="card-body text-center d-flex flex-column align-items-center justify-content-center p-5">
                        <button type="button" class="btn btn-soft-warning rounded-circle btn-icon-only mr-0">
                                <span class="btn-inner--icon">
                                    <i class="fas fa-plus"></i>
                                </span>
                        </button>
                        <div class="mt-4 h6 text-muted">{{ __('Opret nyt ønske') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>