@extends('layouts.app')

@section('content')
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <!-- Circles -->
    @include('partials.circles')
    <!-- Header -->
        <div class="container mb-5">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Title -->
                    <div class="row align-items-center mb-4">
                        <div class="col-md-5 mb-4 mb-md-0">
                            <span class="h2 mb-0 text-white d-block">{{ __('Opret ønske') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Create wish  -->
    <section class="slice bg-section-secondary">
        <div class="container mn-5">
            <div class="row">
                <div class="col-12">
                    <div class="card zindex-100">
                        <div class="card-body px-md-5 py-5">
                            @if($missing_items)
                                <div class="mb-4">
                                    <span class="badge badge-soft-info badge-pill">
                                        Vi kunne desværre ikke finde alle informationer på dit ønske. Udfyld venligst selv de manglende informationer i felterne nedenfor.
                                    </span>
                                </div>
                            @endif
                            <form method="POST" action="{{ route('wishes.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-5">
                                        <div class="form-group">
                                            <label for="wish_list_id"
                                                   class="form-control-label">{{ __('Vælg ønskeliste') }}</label>
                                            <select name="wish_list_id" id="wish_list_id" class="custom-select"
                                                    data-toggle="select" required>
                                                @foreach($wishlists as $wishlist)
                                                    <option value="{{ Crypt::encrypt($wishlist->id) }}"
                                                            @if($wishlist->id === $selected_wishlist->id) selected @endif>{{ $wishlist->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if(Session::get("filepath"))
                                            <div class="form-control-label">{{ __('Billede') }}</div>
                                            <img style="width: 80% !important;"
                                                 src="{!! asset(Session::get("filepath")) !!}">
                                        @else
                                            <div class="form-group">
                                                <label for="image"
                                                       class="form-control-label">{{ __('Billede') }}</label>
                                                <input type="file" name="image" id="image" class="custom-input-file"/>
                                                <label for="image">
                                                    <i class="fa fa-upload"></i>
                                                    <span>{{ __('Tilføj billede') }}</span>
                                                </label>
                                                <small class="form-text text-muted mt-2">{{ __('Tilføj et billede, der viser hvad du ønsker dig') }}</small>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-12 col-md-7">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label">{{ __('Titel') }}</label>
                                            <input type="text" id="name" class="form-control" name="name"
                                                   value="@if($scraper['name']){!! $scraper['name'] !!}@else{{ old('name') }}@endif"
                                                   placeholder="{{__('Giv dit ønske en titel') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description"
                                                   class="form-control-label">{{ __('Beskrivelse') }}</label>
                                            <textarea id="description" class="form-control" name="description"
                                                      placeholder="{{__('Tilføj en beskrivelse til dit ønske. Farve, antal, størrelse eller andre detaljer, der er værdi at vide for dem, der skal købe det') }}">{{ old('description') }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="price" class="form-control-label">{{ __('Pris') }}</label>
                                            <input type="number" id="price" class="form-control" name="price"
                                                   value="@if($scraper['price']){!! $scraper['price'] !!}@else{{ old('price') }}@endif"
                                                   placeholder="{{__('Tilføj en pris') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="link" class="form-control-label">{{ __('Link') }}</label>
                                            <input type="text" id="link" class="form-control" name="link"
                                                   value="@if($link){!! $link !!}@else{{ old('link') }}@endif"
                                                   placeholder="{{__('Tilføj et link, hvis ønsket kan købes online') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-warning">{{ __('Opret ønske') }}</button>
                                    <a href="{{ URL::previous() }}"
                                       class="btn btn-link text-muted">{{ __('Annuller') }}</a>
                                </div>
                                @if ($errors->any())
                                    <div class="small text-danger mt-3" role="alert">
                                        <strong>{{ $errors->first() }}</strong>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection