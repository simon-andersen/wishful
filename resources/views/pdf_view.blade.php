<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>{{ $title }}</title>
</head>
<body>
<p style="font-style: italic;">{{ __('Ønskeliste udskrevet fra Wishful - Opret din egen ønskeliste på www.wishful.dk') }}</p>
<h1>{{ $title }}</h1>
<p>{{ $description }}</p>
<table width="100%" style="width:100%" border="1" cellspacing="0" cellpadding="10">
    <tr>
        <th>{{ __('Billede') }}</th>
        <th>{{ __('Navn') }}</th>
        <th>{{ __('Beskrivelse') }}</th>
        <th>{{ __('Pris') }}</th>
    </tr>
    @foreach($wishes as $wish)
        <tr>
            <td>@isset($wish['image'])<img src="./{{ $wish['image'] }}" width="80" alt="">@endisset</td>
            <td>{{ $wish['name'] }}</td>
            <td>{{ $wish['description'] }}</td>
            <td>{{ $wish['price'] }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>