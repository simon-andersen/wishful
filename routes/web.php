<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
});

Route::get('/home', 'HomeController@index')->name('home');

// Wishlist
Route::resource('wishlists', 'WishListController');
Route::get('wishlists/{wishlist}/print-pdf', 'PrintController@printPDF')->name('wishlist.printpdf');


// Wishes
Route::resource('wishes', 'WishController');
Route::post('wishes/create/{id?}', 'WishController@create')->name('wishes.create');

// Share
Route::get('share/{id}', 'ShareController@show')->name('share.show');
Route::patch('share/reserve/{wish}', 'ShareController@reserve')->name('share.reserve');
Route::patch('share/delete-reserve/{wish}', 'ShareController@deleteReserve')->name('share.delete-reserve');

// Users
Route::get('profile/', 'UserController@show')->name('profile');
Route::patch('profile/{user}/', 'UserController@update')->name('users.update');
Route::patch('profile/password/{user}/', 'UserController@updatePassword')->name('users.update-password');
Route::delete('profile/delete/{user}/', 'UserController@destroy')->name('users.destroy');

// Auth
Auth::routes();
