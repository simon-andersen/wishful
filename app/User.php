<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function wishlists()
    {
        return $this->belongsToMany(WishList::class);
    }

    public function isPartOfWishlist($wishlist)
    {
        return $this->wishlists()->where('wish_list_id', $wishlist->id)->exists();
    }

    public function wishes()
    {
        $wishes = collect();
        foreach($this->wishlists()->get() as $wishlist) {
            foreach($wishlist->wishes as $wish) {
                if($wishes->where('id', $wish->id)->count() === 0) {
                    $wishes->push($wish);
                }
            }
        }
        return $wishes;
    }

    public function isPartOfWish($wish)
    {
        if ($this->wishes()->where('id', $wish->id)->count() === 1) {
            return true;
        } else {
            return false;
        }
    }

}
