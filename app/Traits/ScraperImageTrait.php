<?php

namespace App\Traits;


use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

trait ScraperImageTrait
{
    public function scraperImage($image)
    {
        $url = $image;
        $info = pathinfo($url);
        $contents = file_get_contents($url);
        $file = '/tmp/' . $info['basename'];
        file_put_contents($file, $contents);
        // Get image file
        $image = new UploadedFile($file, $info['basename']);
        // Make a image name based on random str and current timestamp
        $name = str_random(10) . '_' . time();
        // Define folder path
        $folder = public_path('/uploads/images/');
        // Make a file path where image will be stored [ folder path + file name + file extension]
        $filePath = '/uploads/images/' . $name . '.jpg';
        // Save image
        Image::make($image)->encode('jpg')->resizeCanvas(800, 800, 'center', false, 'fff')->save($folder . $name . '.jpg');
        // Session with filepath
        return $filePath;
    }
}