<?php

namespace App\Traits;

use Goutte\Client;

trait ScraperTrait
{
    public function scraper($link)
    {
        $client = new Client();
        $url = $link;
        $crawler = $client->request('GET', $url);
        $wish = (object)[];
        $wish->name = null;
        $wish->image = null;
        $wish->price = null;
        $wish->currency = null;
        $crawler->filterXPath('//*[@type="application/ld+json"]')
            ->each(function ($node) use (&$wish) {
                $item = json_decode(trim($node->text()));
                if (isset($item->name)) {
                    $wish->name = $item->name;
                }
                if (isset($item->image)) {
                    $wish->image = $item->image;
                }
                if (isset($item->offers->price)) {
                    $wish->price = $item->offers->price;
                }
                if (isset($item->offers->priceCurrency)) {
                    $wish->currency = $item->offers->priceCurrency;
                }
                if (isset($item->offers) && is_array($item->offers)) {
                    $wish->price = $item->offers[0]->price;
                    $wish->currency = $item->offers[0]->priceCurrency;
                }
            });
        $crawler->filterXPath("//*[@itemprop and not (@itemscope)]")
            ->each(function ($node) use (&$wish) {
                $ret = $this->getNodeStructuredData($node, 'microdata');
                if ($ret['property'] === 'name' && ($wish->name === null || $wish->name === '')) {
                    $wish->name = $ret['value'];
                } elseif ($ret['property'] === 'image' && ($wish->image === null || $wish->image === '')) {
                    $wish->image = $ret['value'];
                } elseif ($ret['property'] === 'price' && ($wish->price === null || $wish->price === '')) {
                    $wish->price = $ret['value'];
                } elseif ($ret['property'] === 'priceCurrency' && ($wish->currency === null || $wish->currency === '')) {
                    $wish->currency = $ret['value'];
                }
            });
        $crawler->filterXPath("//head/meta[starts-with(@property, 'og')]")
            ->each(function ($node) use (&$wish) {
                if ($node->attr('property') === 'og:title' && ($wish->name === null || $wish->name === '')) {
                    $wish->name = $node->attr('content');
                } elseif ($node->attr('property') === 'og:image' && ($wish->image === null || $wish->image === '')) {
                    $wish->image = $node->attr('content');
                } elseif ($node->attr('property') === 'og:price:amount' && ($wish->price === null || $wish->price === '')) {
                    $wish->price = $node->attr('content');
                } elseif ($node->attr('property') === 'og:price:currency' && ($wish->currency === null || $wish->currency === '')) {
                    $wish->currency = $node->attr('content');
                }
            });
        return json_decode(json_encode($wish), true);
    }

    private function getNodeStructuredData($node, $type = 'microdata')
    {
        $node_name = $node->nodeName();
        if ($node_name == 'link' || $node_name == 'a') {
            $value = $node->attr('href');
        } elseif ($node_name == 'img') {
            $value = $node->attr('src');
        } elseif ($node_name == 'meta') {
            $value = $node->attr('content');
        } else {
            $value = trim($node->text());
        }
        if ($type == 'microdata') {
            $property = current($node->extract(['itemprop']));
        } elseif ($type == 'rdfa') {
            $property = current($node->extract(['property']));
        }

        return [
            'property' => $property,
            'value'    => $value,
        ];
    }
}