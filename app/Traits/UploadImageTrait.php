<?php

namespace App\Traits;


use Intervention\Image\Facades\Image;

trait UploadImageTrait
{
    public function uploadImage($image)
    {
        // Make a image name based on random str and current timestamp
        $name = str_random(10) . '_' . time();
        // Define folder path
        $folder = public_path('/uploads/images/');
        // Make a file path where image will be stored [ folder path + file name + file extension]
        $filePath = '/uploads/images/' . $name . '.jpg';
        // Upload image
        Image::make($image)->encode('jpg')->resizeCanvas(800, 800, 'center', false, 'fff')->save($folder . $name . '.jpg');
        // Set user profile image path in database to filePath
        return $filePath;
    }
}