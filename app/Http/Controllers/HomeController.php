<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            $wishlists = Auth::User()->wishlists()->orderBy('created_at', 'desc')->get();
            return view('home', compact('wishlists'));
        } else {
            return abort(403);
        }
    }
}
