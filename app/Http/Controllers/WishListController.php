<?php

namespace App\Http\Controllers;

use App\WishList;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        return view('wishlists.create');
    }

    public function store()
    {
        $wishlist = new WishList;
        $wishlist->fill(request()->all());
        $wishlist->share_id = uniqid();
        $wishlist->save();
        $wishlist->users()->attach(Auth::user()->id);

        session()->flash('success', __('Ønskelisten blev oprettet.'));
        return redirect('/home');
    }

    public function show(WishList $wishlist)
    {
        if (Auth::User()->isPartOfWishlist($wishlist)) {
            $wishes = $wishlist->wishes()->orderBy('created_at', 'desc')->get();
            return view('wishlists.show')->with('wishlist', $wishlist)->with('wishes', $wishes);
        } else {
            return abort(403);
        }
    }

    public function edit(WishList $wishlist)
    {
        if (Auth::User()->isPartOfWishlist($wishlist)) {
            return view('wishlists.edit', compact('wishlist'));
        } else {
            return abort(403);
        }
    }

    public function update(WishList $wishlist)
    {
        if (Auth::User()->isPartOfWishlist($wishlist)) {
            $wishlist->fill(request()->all());
            $wishlist->save();

            session()->flash('success', __('Ønskelisten blev opdateret.'));
            return redirect('/wishlists/' . $wishlist->id);
        } else {
            return abort(403);
        }
    }

    public function destroy(WishList $wishlist)
    {
        if (Auth::User()->isPartOfWishlist($wishlist)) {
            $wishlist->users()->detach();
            $wishlist->wishes()->delete();
            $wishlist->delete();

            session()->flash('success', __('Ønskelisten blev slettet.'));
            return redirect('/home');
        } else {
            return abort(403);
        }
    }

    public function printPDF(WishList $wishlist)
    {
        $data = [
            'title' => $wishlist->name,
            'description' => $wishlist->description,
            'wishes' => $wishlist->wishes->toArray(),
            'users' => $wishlist->users->toArray(),
        ];
        $pdf = PDF::loadView('pdf_view', $data);
        return $pdf->download($wishlist->name.'.pdf');
    }
}
