<?php

namespace App\Http\Controllers;

use App\Traits\ScraperImageTrait;
use App\Traits\ScraperTrait;
use App\Traits\UploadImageTrait;
use App\Wish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;

class WishController extends Controller
{
    use ScraperImageTrait;
    use ScraperTrait;
    use UploadImageTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        abort(404);
    }

    public function create(Request $request)
    {
        if (request('link')) {
            $scraper = $this->scraper(request('link'));
            $link = request('link');
            if ($scraper['image']) {
                // Upload scraper image and return file path
                $filePath = $this->scraperImage($scraper['image']);
                // Create session with file path
                $request->session()->put('filepath', $filePath);
            } else {
                $request->session()->forget('filepath');
            }
        } else {
            $request->session()->forget('filepath');
            $scraper = null;
            $link = null;
        }

        $wishListRequest = Auth::User()->wishlists->where('id', request('wishlist'))->first();
        if ($wishListRequest) {
            $selected_wishlist = $wishListRequest;
        } else {
            $selected_wishlist = Auth::User()->wishlists->last();
        }

        $wishlists = Auth::User()->wishlists()->orderBy('created_at', 'desc')->get();

        $missing_items = false;
        if($scraper['name'] === null || $scraper['image'] === null || $scraper['price'] === null) {
            $missing_items = true;
        }

        return view('wishes.create')->with('wishlists', $wishlists)->with('selected_wishlist',
            $selected_wishlist)->with('scraper', $scraper)->with('link', $link)->with('missing_items', $missing_items);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $wish = new Wish;
        $wish->fill(request()->all());
        $wish->wish_list_id = Crypt::decrypt(request('wish_list_id'));
        if ($request->has('image')) {
            // Upload image from request
            $wish->image = $this->uploadImage($request->file('image'));
        } elseif ($request->session()->get('filepath')) {
            // Upload image from session (from scraper)
            $wish->image = $request->session()->get('filepath');
        }
        $wish->save();

        session()->flash('success', __('Ønsket blev oprettet.'));
        return redirect('/wishlists/' . Crypt::decrypt(request('wish_list_id')));
    }

    public function show()
    {
        return abort(404);
    }

    public function edit(Wish $wish)
    {
        if (Auth::User()->isPartOfWish($wish)) {
            $wishlists = Auth::User()->wishlists()->orderBy('created_at', 'desc')->get();

            return view('wishes.edit')->with('wish', $wish)->with('wishlists', $wishlists);
        } else {
            return abort(403);
        }
    }

    public function update(Wish $wish)
    {
        if (Auth::User()->isPartOfWish($wish)) {
            $wish->fill(request()->all());
            $wish->wish_list_id = Crypt::decrypt(request('wish_list_id'));
            $wish->save();

            session()->flash('success', __('Ønsket blev opdateret.'));
            return redirect('/wishlists/' . $wish->wish_list_id);
        } else {
            return abort(403);
        }
    }

    public function destroy(Wish $wish)
    {
        if (Auth::User()->isPartOfWish($wish)) {
            $filePath = public_path() . $wish->image;
            if (file_exists($filePath)) {
                File::delete($filePath);
            }
            $wish->delete();

            session()->flash('success', __('Ønsket blev slettet.'));
            return back();
        } else {
            return abort(403);
        }
    }
}
