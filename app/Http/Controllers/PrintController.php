<?php

namespace App\Http\Controllers;

use App\WishList;
use Barryvdh\DomPDF\Facade as PDF;
class PrintController extends Controller
{

    public function printPDF(WishList $wishlist)
    {
        $data = [
            'title' => $wishlist->name,
            'description' => $wishlist->description,
            'wishes' => $wishlist->wishes->toArray(),
            'users' => $wishlist->users->toArray(),
        ];
        $pdf = PDF::loadView('pdf_view', $data);
        return $pdf->download($wishlist->name.'.pdf');
    }
}
