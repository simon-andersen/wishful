<?php

namespace App\Http\Controllers;

use App\Wish;
use App\WishList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShareController extends Controller
{
    public function show($id)
    {
        $wishlist = Wishlist::where('share_id', $id)->first();
        if ($wishlist && Auth::check() && Auth::User()->isPartOfWishlist($wishlist)) {
            return abort(403, 'Hov! Dette er din egen ønskeliste.');
        } else if ($wishlist){
            return view('share.show', compact('wishlist'));
        } else {
            return abort(404);
        }
    }

    public function reserve(Wish $wish)
    {
        $wish->reserved = 1;
        $wish->reserved_by = Auth::User()->name;
        $wish->save();

        return back();
    }

    public function deleteReserve(Wish $wish)
    {
        $wish->reserved = 0;
        $wish->reserved_by = null;
        $wish->save();

        return back();
    }
}
