<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show() {
        $user = Auth::User();
        return view('users.show', compact('user'));
    }

    public function update(User $user) {
        if ($user->id === Auth::User()->id) {
            $user->fill(request()->all());
            $user->date_of_birth = date('Y-m-d', strtotime(request('date_of_birth')));
            $user->save();
            session()->flash('success', __('Dine profiloplysninger blev ændret.'));
            return redirect('/profile');
        } else {
            session()->flash('error', __('Du har ikke adgang til at ændre denne bruger.'));
            return redirect('/profile');
        }
    }

    public function updatePassword(User $user) {
        if ($user->id === Auth::User()->id) {
            if (strlen(request('password'))) {
                $user->password = Hash::make(request('password'));
            }
            $user->save();
            session()->flash('profile-success', __('Din adgangskode blev ændret.'));
            return redirect('/profile');
        } else {
            session()->flash('profile-error', __('Du har ikke adgang til at ændre denne bruger.'));
            return redirect('/profile');
        }
    }

    public function destroy(User $user) {
        if ($user->id === Auth::User()->id) {
            $user->delete();
            session()->flash('success', __('Brugeren blev slettet.'));
            return redirect('/');
        } else {
            session()->flash('error', __('Du har ikke adgang til at slette denne bruger.'));
            return redirect('/profile');
        }
    }
}
