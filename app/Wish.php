<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wish extends Model
{
    protected $fillable = [
        'name', 'description', 'price', 'link',
    ];

    public function wishlist()
    {
        return $this->belongsTo(Wishlist::class);
    }
}
